package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"os/exec"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"github.com/namsral/flag"
	log "github.com/sirupsen/logrus"
)

const (
	AppVersion = "v1.0.0"
)

var (
	gitlabURL   = ""
	runnerToken = ""
)

func init() {
	log.Printf("cold-start gitlab-lambda-runner %s", AppVersion)
	flag.StringVar(&gitlabURL, "gitlab_url", "https://gitlab.com", "URL of the gitlab instance to connect to")
	flag.StringVar(&runnerToken, "runner_token", "", "Runner token to use when connecting to the gitlab instance")
	flag.Parse()
}

func main() {
	log.Printf("warm-start gitlab-lambda-runner %s", AppVersion)

	localExec, _ := os.LookupEnv("LOCAL_EXEC")
	if localExec == "true" {
		log.Println("starting local execution, as LOCAL_EXEC is set to true")

		HandleLocally()
	} else {
		log.Println("starting lambda execution on AWS")

		lambda.Start(HandleRequest)
	}
}

// HandleLocally is the local test equivalent to HandleRequest
func HandleLocally() {
	ExecuteRun()
}

// HandleRequest is the lambda function Handler
func HandleRequest(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	err := ExecuteRun()

	if err != nil {
		return events.APIGatewayProxyResponse{Body: string("gitlab-runner runtime error"), StatusCode: 500}, nil
	}
	return events.APIGatewayProxyResponse{Body: string("gitlab-runner finished successfully"), StatusCode: 200}, nil

}

// ExecuteRun executes a gitlab-runner single run
func ExecuteRun() error {

	// use the run-single command with local shell executor
	cmd := exec.Command("gitlab-runner", "run-single", "-u", gitlabURL, "-t", runnerToken, "--executor", "shell", "--builds-dir", "/tmp/builds", "--cache-dir", "/tmp/cache", "--max-builds", "1", "--wait-timeout", "1")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Error(err)
		return err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		log.Error(err)
		return err
	}

	if err := cmd.Start(); err != nil {
		log.Error(err)
		return err
	}
	log.Info(fmt.Sprintf("Executing runner: %s", cmd.Args))

	logScanner := bufio.NewScanner(stderr)
	logScanner.Split(bufio.ScanLines)
	for logScanner.Scan() {
		line := logScanner.Text()
		log.Println(line)
	}

	if err := cmd.Wait(); err != nil {
		log.Error(err)
		return err
	}
	log.Info("Finished runner execution")

	log.Info(fmt.Sprintf("%s", stdout))
	return nil
}
